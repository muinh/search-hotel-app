<?php

namespace App\Service;

use App\Repository\{BookingRepository, HotelRepository};
use DateTime;

/**
 * Class SearchDataService
 *
 * @package App\Service
 */
class SearchDataService
{
    /**
     * @var HotelRepository
     */
    private $hotelRepository;

    /**
     * @var BookingRepository
     */
    private $bookingRepository;

    /**
     * @var DiscountService
     */
    private $discountService;

    /**
     * @var OrderCalculatorService
     */
    private $orderCalculatorService;

    /**
     * SearchDataService constructor.
     *
     * @param HotelRepository $hotelRepository
     * @param BookingRepository $bookingRepository
     * @param DiscountService $discountService
     * @param OrderCalculatorService $orderCalculatorService
     */
    public function __construct(
        HotelRepository $hotelRepository,
        BookingRepository $bookingRepository,
        DiscountService $discountService,
        OrderCalculatorService $orderCalculatorService
    ) {
        $this->hotelRepository = $hotelRepository;
        $this->bookingRepository = $bookingRepository;
        $this->discountService = $discountService;
        $this->orderCalculatorService = $orderCalculatorService;
    }

    /**
     * Get hotels data for search page.
     *
     * @param array $filters
     * @return array
     * @throws \Exception
     */
    public function getHotelsDataForSearchPage(array $filters) : array
    {
        $hotels = $this->hotelRepository->getFiltered($filters)->toArray();

        try {
            $departDate = new DateTime($filters['depart_date']);
            $returnDate = new DateTime($filters['return_date']);
            $totalDays = $returnDate->diff($departDate)->days + 1;
            $discounts = $this->discountService->getDiscountsForPeriod($departDate, $returnDate);
        } catch (\Exception $exception) {
            echo $exception->getMessage();
            exit;
        }

        $bookedRooms = $this->bookingRepository->getBookedForPeriod($departDate, $returnDate);

        foreach ($hotels as $hotel) {
            if (\array_key_exists($hotel->id, $discounts)) {
                $hotel->discounts = $discounts[$hotel->id];
            }

            $hotel->orderDetails = $this->orderCalculatorService->getOrderData(
                $hotel->price_per_room,
                $totalDays,
                $hotel->discounts ?? null
            );

            $bookedRooms->map(function ($booking) use($hotel) {
                if ($hotel->id === $booking->hotel_id) {
                    $hotel->bookedOnThisPeriod = $booking->bookedOnThisPeriod;
                    $hotel->hasRooms = $hotel->rooms_qty > $hotel->bookedOnThisPeriod;
                }

                return $booking;
            });
        }

        return $hotels;
    }
}
