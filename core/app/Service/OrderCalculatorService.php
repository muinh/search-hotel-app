<?php

namespace App\Service;

/**
 * Class OrderCalculatorService
 *
 * @package App\Service
 */
class OrderCalculatorService
{
    /**
     * Get total amount for all days.
     *
     * @param float $price
     * @param int $days
     * @param array|null $discounts
     * @return array
     */
    public function getOrderData(float $price, int $days, ?array $discounts) : array
    {
        $totalDiscountDays = 0;
        $totalDiscountAmount = 0;

        if ($discounts !== null) {
            foreach ($discounts as $discount) {
                $discountDays = $discount['discountDays'];
                $totalDiscountAmount += $this->getAmountForDiscountDays($price, $discountDays, $discount['discount']);
                $totalDiscountDays += $discountDays;
            }
        }

        $leftDays = $days - $totalDiscountDays;
        $regularAmount = $this->getAmount($price, $leftDays);

        return [
            'totalAmount' => $totalDiscountAmount + $regularAmount,
            'totalDays' => $days,
            ''
        ];
    }

    /**
     * Get amount for discount days.
     *
     * @param float $price
     * @param int $days
     * @param float $discount
     * @return float
     */
    protected function getAmountForDiscountDays(float $price, int $days, float $discount) : float
    {
        $discountedPrice = $price - ($price * $discount / 100);

        return $this->getAmount($discountedPrice, $days);
    }

    /**
     * Get amount for regular days.
     *
     * @param float $price
     * @param int $days
     * @return float
     */
    protected function getAmount(float $price, int $days) : float
    {
        return $price * $days;
    }
}
