<?php

namespace App\Service;

use App\Models\Discount;
use DateTime;

/**
 * Class DiscountService
 */
class DiscountService
{
    /**
     * Get discounts for period.
     *
     * @param DateTime $departDate
     * @param DateTime $returnDate
     * @return array
     * @throws \Exception
     */
    public function getDiscountsForPeriod(DateTime $departDate, DateTime $returnDate) : array
    {
        $discountsData = [];
        $discounts = Discount::all();

        foreach ($discounts as $discount) {
            $hotelId = $discount->hotel_id;
            try {
                $discountStart = new DateTime($discount->start_date);
                $discountEnd = new DateTime($discount->end_date);
            } catch (\Exception $exception) {
                // ideally redirect to user friendly page with error explanation.
                echo $exception->getMessage();
                exit;
            }

            if ($discountStart <= $returnDate && $discountEnd >= $departDate) {
                $discountDays = min($discountEnd, $returnDate)->diff(max($departDate, $discountStart))->days + 1;
                $discountsData[$hotelId][] = [
                    'discount' => $discount->discount_size,
                    'discountDays' => $discountDays,
                ];
            }
        }

        return $discountsData;
    }
}
