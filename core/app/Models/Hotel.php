<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    /**
     * @var array
     */
    private $discounts;

    /**
     * Get discounts.
     *
     * @return array
     */
    public function getDiscounts() : array
    {
        return $this->discounts;
    }

    /**
     * Set discounts.
     *
     * @param array $discounts
     * @return Hotel
     */
    public function setDiscounts(array $discounts) : Hotel
    {
        $this->discounts = $discounts;

        return $this;
    }
}
