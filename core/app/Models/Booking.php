<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    private $id;

    /**
     * Get id.
     *
     * @return mixed
     */
    public function getId() : int
    {
        return $this->id;
    }
}
