<?php

namespace App\Repository;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class BookingRepository
 *
 * @package App\Repository
 */
class BookingRepository
{
    /**
     * Get booked hotel rooms for period.
     *
     * @param $departDate
     * @param $returnDate
     * @return Collection
     */
    public function getBookedForPeriod($departDate, $returnDate) : Collection
    {
        return DB::table('bookings as b')
            ->selectRaw('b.hotel_id, COUNT(b.id) as bookedOnThisPeriod')
            ->whereBetween('start_date', [$departDate, $returnDate])
            ->orWhereBetween('end_date', [$departDate, $returnDate])
            ->groupBy('hotel_id')
            ->get();
    }
}
