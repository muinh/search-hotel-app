<?php

namespace App\Repository;

use Illuminate\Support\{Arr, Collection};
use Illuminate\Support\Facades\DB;

/**
 * Class HotelRepository
 */
class HotelRepository
{
    /**
     * Get filtered.
     *
     * @param array $filters
     * @return Collection
     */
    public function getFiltered(array $filters)
    {
        $db = DB::table('hotels as h');

        if (Arr::has($filters, ['term', 'depart_date', 'return_date'])) {
            $term = Arr::get($filters, 'term');

            $db->where('name', 'LIKE', '%' . $term . '%')
                ->orWhere('country', 'LIKE', '%' . $term . '%')
                ->orWhere('city', 'LIKE', '%' . $term . '%');
        }

        return $db->get();
    }
}
