<?php

namespace App\Http\Controllers;

use App\Service\SearchDataService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

/**
 * Class SearchController
 *
 * @package App\Http\Controllers
 *
 */
class SearchController extends Controller
{
    /**
     * Get results by term.
     *
     * @param Request $request
     * @param SearchDataService $searchDataService
     * @return View
     * @throws \Exception
     */
    public function getByTerm(Request $request, SearchDataService $searchDataService) : View
    {
        $validator = Validator::make($request->all(), [
            'term' => 'required',
            'depart_date' => 'required|date',
            'return_date' => 'required|date|after:depart_date'
        ]);

        if ($validator->fails()) {
            $errors = $validator->messages()->get('*');

            return view('search', [
                'errors' => array_map('array_pop', $errors)
            ]);
        }

        return view ('search', [
            'hotels' => $searchDataService->getHotelsDataForSearchPage($request->request->all())
        ]);
    }
}
