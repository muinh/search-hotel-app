## Instructions

`git pull`

`cd docker-config`

`docker-compose up -d`

`docker exec -it app bash`

`composer install`

`cp .env.example .env`

`php artisan key:generate`

`Change database settings in your .env`
```
DB_CONNECTION=mysql
DB_HOST=db
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=laraveluser
DB_PASSWORD=rootpwd
```

`php artisan migrate:fresh --seed`

`Now you are set.` 

```
This is the main page:
GET http://localhost/
```

```
You can search for accomodation by specifying search field, depart and return dates.
In case of getting maximal experience user needs to specify all of these fields.
They are required.

Dummy data was seeded into your database. 
If you need more experience with test data feel free to seed more.

There is a search for hotel name, address and destination for hotels.

Script also shows the number of rooms left in each hotel.
If no rooms left, it will inform you as well.
There is no flexibility in room bookings for this application version. 
e.g. if room is booking even for one day for the needed period, it will be unavailable for booking.

Total price for all nights is computed by analyzing all the discounts for the hotel.
If only a few days from the needed period fit the discount period, it will compute these days by discount price.

If there are some questions, please contact me.
dmytropopov.ua@gmail.com
```
