<html>
<head>
    <link rel="stylesheet" href="main.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        @if(isset($errors))
            @foreach($errors as $error)
                <div class="col-md-12 alert alert-danger" role="alert">
                    {{ $error }}
                </div>
            @endforeach
        @endif
    </div>
</div>
<div class="s002">
    <form action="{{ url('/') }}">
        <div class="inner-form">
            <div class="input-field first-wrap">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z"></path>
                    </svg>
                </div>
                <input name="term" id="search" type="text" placeholder="What are you looking for?" required/>
            </div>
            <div class="input-field second-wrap">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z"></path>
                    </svg>
                </div>
                <input name="depart_date" type="date" placeholder="01 Mar 2020" required/>
            </div>
            <div class="input-field third-wrap">
                <div class="icon-wrap">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path d="M17 12h-5v5h5v-5zM16 1v2H8V1H6v2H5c-1.11 0-1.99.9-1.99 2L3 19c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2V5c0-1.1-.9-2-2-2h-1V1h-2zm3 18H5V8h14v11z"></path>
                    </svg>
                </div>
                <input name="return_date" type="date" placeholder="31 Dec 2020" required/>
            </div>
            <div class="input-field fifth-wrap">
                <input class="btn-search" type="submit" />
            </div>
        </div>
    </form>
</div>
<table class="table table-striped">
    <thead class="thead-dark">
    <tr>
        <th>Hotel Name</th>
        <th>Address</th>
        <th>Country</th>
        <th>City</th>
        <th>Rooms left</th>
        <th>Price</th>
    </tr>
    </thead>
    <tbody>
        @if(isset($hotels) and count($hotels))
            @foreach($hotels as $hotel)
                <tr>
                    <td>{{ $hotel->name }}</td>
                    <td>{{ $hotel->full_address }}</td>
                    <td>{{ $hotel->country }}</td>
                    <td>{{ $hotel->city }}</td>
                    <td>
                        @if($hotel->hasRooms)
                            <span>{{ $hotel->rooms_qty - $hotel->bookedOnThisPeriod }}</span>
                            <span> from {{ $hotel->rooms_qty }}</span>
                        @else
                            <span>No rooms left</span>
                        @endif
                    </td>
                    <td>
                        @if($hotel->hasRooms)
                            {{ $hotel->orderDetails['totalAmount'] }} for all {{ $hotel->orderDetails['totalDays'] }} nights.
                            @if(isset($hotel->discounts))
                                @foreach($hotel->discounts as $discount)
                                    <p>({{ $discount['discount'] }}% off for {{ $discount['discountDays'] }} nights included)</p>
                                    <p>
                                        Regular price:
                                        <span class="standard-price">
                                            {{ $hotel->price_per_room }}
                                        </span>
                                    </p>
                                    <p>
                                        Discount price:
                                        <span class="discount-price">
                                            {{ $hotel->price_per_room - ($hotel->price_per_room * $discount['discount'] / 100)}}
                                            for {{ $discount['discountDays'] }} nights
                                        </span>
                                    </p>
                                @endforeach
                            @else
                                <p>
                                    Regular price for one night:
                                    <span>{{ $hotel->price_per_room }}</span>
                                </p>
                            @endif
                        @else
                            <span>No rooms left</span>
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr style="text-align: center">
                <td colspan="6">No hotels found.</td>
            </tr>
        @endif
    </tbody>
</table>
<script src="js/extention/choices.js"></script>
<script src="js/extention/flatpickr.js"></script>
<script>
    flatpickr(".datepicker",
        {});
</script>
<script>
    const choices = new Choices('[data-trigger]',
        {
            searchEnabled: false,
            itemSelectText: '',
        });

</script>
</body>
</html>
