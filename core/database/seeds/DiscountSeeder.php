<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DiscountSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('discounts')->insert([
            'hotel_id' => 1,
            'discount_size' => 20,
            'start_date' => new Carbon('first day of March 2020'),
            'end_date' => new Carbon('last day of May 2020'),
        ]);

        DB::table('discounts')->insert([
            'hotel_id' => 1,
            'discount_size' => 10,
            'start_date' => new Carbon('first day of June 2020'),
            'end_date' => new Carbon('last day of August 2020'),
        ]);

        DB::table('discounts')->insert([
            'hotel_id' => 1,
            'discount_size' => 25,
            'start_date' => new Carbon('first day of September 2020'),
            'end_date' => new Carbon('last day of December 2020'),
        ]);

        DB::table('discounts')->insert([
            'hotel_id' => 2,
            'discount_size' => 50,
            'start_date' => new Carbon('first day of March 2020'),
            'end_date' => new Carbon('last day of May 2020'),
        ]);

        DB::table('discounts')->insert([
            'hotel_id' => 2,
            'discount_size' => 45,
            'start_date' => new Carbon('first day of June 2020'),
            'end_date' => new Carbon('last day of June 2020'),
        ]);

        DB::table('discounts')->insert([
            'hotel_id' => 2,
            'discount_size' => 40,
            'start_date' => new Carbon('first day of July 2020'),
            'end_date' => new Carbon('last day of July 2020'),
        ]);

        DB::table('discounts')->insert([
            'hotel_id' => 2,
            'discount_size' => 35,
            'start_date' => new Carbon('first day of August 2020'),
            'end_date' => new Carbon('last day of August 2020'),
        ]);

        DB::table('discounts')->insert([
            'hotel_id' => 2,
            'discount_size' => 30,
            'start_date' => new Carbon('first day of September 2020'),
            'end_date' => new Carbon('last day of September 2020'),
        ]);

        DB::table('discounts')->insert([
            'hotel_id' => 2,
            'discount_size' => 25,
            'start_date' => new Carbon('first day of October 2020'),
            'end_date' => new Carbon('last day of October 2020'),
        ]);

        DB::table('discounts')->insert([
            'hotel_id' => 2,
            'discount_size' => 20,
            'start_date' => new Carbon('first day of November 2020'),
            'end_date' => new Carbon('last day of November 2020'),
        ]);

        DB::table('discounts')->insert([
            'hotel_id' => 2,
            'discount_size' => 15,
            'start_date' => new Carbon('first day of December 2020'),
            'end_date' => new Carbon('last day of December 2020'),
        ]);

        DB::table('discounts')->insert([
            'hotel_id' => 3,
            'discount_size' => 80,
            'start_date' => new Carbon('first day of July 2020'),
            'end_date' => new Carbon('last day of August 2020'),
        ]);

        DB::table('discounts')->insert([
            'hotel_id' => 4,
            'discount_size' => 3,
            'start_date' => new Carbon('first day of September 2020'),
            'end_date' => new Carbon('last day of November 2020'),
        ]);

        DB::table('discounts')->insert([
            'hotel_id' => 4,
            'discount_size' => 13,
            'start_date' => new Carbon('first day of June 2020'),
            'end_date' => new Carbon('last day of June 2020'),
        ]);

        DB::table('discounts')->insert([
            'hotel_id' => 5,
            'discount_size' => 34,
            'start_date' => new Carbon('first day of May 2020'),
            'end_date' => new Carbon('last day of May 2020'),
        ]);

        DB::table('discounts')->insert([
            'hotel_id' => 5,
            'discount_size' => 34,
            'start_date' => new Carbon('first day of October 2020'),
            'end_date' => new Carbon('last day of October 2020'),
        ]);
    }
}
