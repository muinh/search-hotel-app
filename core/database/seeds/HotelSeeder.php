<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HotelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('hotels')->insert([
            'id' => 1,
            'name' => 'Hyatt',
            'full_address' => 'Berlin-Charlottenburg, Am Schillertheater 4',
            'country' => 'Germany',
            'city' => 'Berlin',
            'price_per_room' => 150,
            'rooms_qty' => 30,
            'created_at' => Carbon::now()
        ]);

        DB::table('hotels')->insert([
            'id' => 2,
            'name' => 'Kalypso',
            'full_address' => 'Mumbai, Maharashtra, 400072, India',
            'country' => 'India',
            'city' => 'Mumbai',
            'price_per_room' => 60,
            'rooms_qty' => 25,
            'created_at' => Carbon::now()
        ]);

        DB::table('hotels')->insert([
            'id' => 3,
            'name' => 'Emerald',
            'full_address' => '1918 F Street, NW Washington, DC 20052',
            'country' => 'USA',
            'city' => 'Washington',
            'price_per_room' => 120,
            'rooms_qty' => 15,
            'created_at' => Carbon::now()
        ]);

        DB::table('hotels')->insert([
            'id' => 4,
            'name' => 'Green Garden',
            'full_address' => 'Liverpool, Main Street 16 b.12',
            'country' => 'United Kingdom',
            'city' => 'Liverpool',
            'price_per_room' => 44,
            'rooms_qty' => 28,
            'created_at' => Carbon::now()
        ]);

        DB::table('hotels')->insert([
            'id' => 5,
            'name' => 'Gorgeous',
            'full_address' => 'Lisbon Rua João das Regras',
            'country' => 'Portugal',
            'city' => 'Lisbon',
            'price_per_room' => 63,
            'rooms_qty' => 32,
            'created_at' => Carbon::now()
        ]);
    }
}
