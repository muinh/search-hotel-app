<?php

use Carbon\Carbon;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BookingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $factory = new Factory;
        $faker = $factory::create();
        for ($i = 0; $i < 100; $i++) {
            $date = $faker->dateTimeBetween($startDate = '2020-03-01', $endDate = '2020-12-31');
            DB::table('bookings')->insert([
                'hotel_id' => 1,
                'start_date' => $date,
                'end_date' => Carbon::parse($date)->addDays(rand(1, 30))->format('Y-m-d H:i:s'),
                'created_at' => Carbon::now()
            ]);
        }

        for ($i = 0; $i < 200; $i++) {
            $date = $faker->dateTimeBetween($startDate = '2020-03-01', $endDate = '2020-12-31');
            DB::table('bookings')->insert([
                'hotel_id' => 2,
                'start_date' => $date,
                'end_date' => Carbon::parse($date)->addDays(rand(1, 30))->format('Y-m-d H:i:s'),
                'created_at' => Carbon::now()
            ]);
        }

        for ($i = 0; $i < 150; $i++) {
            $date = $faker->dateTimeBetween($startDate = '2020-03-01', $endDate = '2020-12-31');
            DB::table('bookings')->insert([
                'hotel_id' => 3,
                'start_date' => $date,
                'end_date' => Carbon::parse($date)->addDays(rand(1, 30))->format('Y-m-d H:i:s'),
                'created_at' => Carbon::now()
            ]);
        }

        for ($i = 0; $i < 200; $i++) {
            $date = $faker->dateTimeBetween($startDate = '2020-03-01', $endDate = '2020-12-31');
            DB::table('bookings')->insert([
                'hotel_id' => 4,
                'start_date' => $date,
                'end_date' => Carbon::parse($date)->addDays(rand(1, 30))->format('Y-m-d H:i:s'),
                'created_at' => Carbon::now()
            ]);
        }

        for ($i = 0; $i < 120; $i++) {
            $date = $faker->dateTimeBetween($startDate = '2020-03-01', $endDate = '2020-12-31');
            DB::table('bookings')->insert([
                'hotel_id' => 5,
                'start_date' => $date,
                'end_date' => Carbon::parse($date)->addDays(rand(1, 30))->format('Y-m-d H:i:s'),
                'created_at' => Carbon::now()
            ]);
        }
    }
}
